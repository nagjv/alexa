Currency Conversion skill lets you know the converted currency value from Country A to Country B. For example, you can ask currency conversion from usa to india. Currency Conversion skill uses two free APIs to get conversion rates. *api.fixer.io* is the default service used for currency conversion, if it is unavaiable, I use *free.currencyconverterapi.com* as backup service.

**Visit Amazon Alexa Skills page for additional details:** [Currency Conversion](https://www.amazon.com/nagjv-Currency-Conversion/dp/B072BPD2P9/ref=sr_1_1?s=digital-skills&ie=UTF8&qid=1498661181&sr=1-1&keywords=currency+conversion)

**alexaskill_cc.py** is the main python script used for the skill as part of lambda function.

**update_lambda.sh** is a simple utility script that does 3 things:

1. Delete any previous zip files used to update aws lambda function
2. Create zip file
3. Update lambda function

**Usage:** *update_lambda.sh needs two parameters. Example: ./update_lambda.sh lambda-function-name zip-file-name*
