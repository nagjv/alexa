import json
import requests

# currency-codes currency mapping - courtesy XE.com
CODES_CURRENCY = {"AED": "UNITED ARAB EMIRATES DIRHAM", "AFN": "AFGHANISTAN AFGHANI", "ALL": "ALBANIA LEK",
                  "AMD": "ARMENIA DRAM", "ANG": "NETHERLANDS ANTILLES GUILDER", "AOA": "ANGOLA KWANZA",
                  "ARS": "ARGENTINA PESO", "AUD": "AUSTRALIA DOLLAR", "AWG": "ARUBA GUILDER",
                  "AZN": "AZERBAIJAN NEW MANAT", "BAM": "BOSNIA AND HERZEGOVINA CONVERTIBLE MARKA",
                  "BBD": "BARBADOS DOLLAR", "BDT": "BANGLADESH TAKA", "BGN": "BULGARIA LEV", "BHD": "BAHRAIN DINAR",
                  "BIF": "BURUNDI FRANC", "BMD": "BERMUDA DOLLAR", "BND": "BRUNEI DARUSSALAM DOLLAR",
                  "BOB": "BOLIVIA BOLÍVIANO", "BRL": "BRAZIL REAL", "BSD": "BAHAMAS DOLLAR",
                  "BTN": "BHUTAN NGULTRUM", "BWP": "BOTSWANA PULA", "BYN": "BELARUS RUBLE", "BZD": "BELIZE DOLLAR",
                  "CAD": "CANADA DOLLAR", "CDF": "CONGO/KINSHASA FRANC", "CHF": "SWITZERLAND FRANC",
                  "CLP": "CHILE PESO", "CNY": "CHINA YUAN RENMINBI", "COP": "COLOMBIA PESO",
                  "CRC": "COSTA RICA COLON", "CUC": "CUBA CONVERTIBLE PESO", "CUP": "CUBA PESO",
                  "CVE": "CAPE VERDE ESCUDO", "CZK": "CZECH REPUBLIC KORUNA", "DJF": "DJIBOUTI FRANC",
                  "DKK": "DENMARK KRONE", "DOP": "DOMINICAN REPUBLIC PESO", "DZD": "ALGERIA DINAR",
                  "EGP": "EGYPT POUND", "ERN": "ERITREA NAKFA", "ETB": "ETHIOPIA BIRR",
                  "EUR": "EURO MEMBER COUNTRIES", "FJD": "FIJI DOLLAR", "FKP": "FALKLAND ISLANDS (MALVINAS) POUND",
                  "GBP": "UNITED KINGDOM POUND", "GEL": "GEORGIA LARI", "GGP": "GUERNSEY POUND",
                  "GHS": "GHANA CEDI", "GIP": "GIBRALTAR POUND", "GMD": "GAMBIA DALASI", "GNF": "GUINEA FRANC",
                  "GTQ": "GUATEMALA QUETZAL", "GYD": "GUYANA DOLLAR", "HKD": "HONG KONG DOLLAR",
                  "HNL": "HONDURAS LEMPIRA", "HRK": "CROATIA KUNA", "HTG": "HAITI GOURDE", "HUF": "HUNGARY FORINT",
                  "IDR": "INDONESIA RUPIAH", "ILS": "ISRAEL SHEKEL", "IMP": "ISLE OF MAN POUND",
                  "INR": "INDIA RUPEE", "IQD": "IRAQ DINAR", "IRR": "IRAN RIAL", "ISK": "ICELAND KRONA",
                  "JEP": "JERSEY POUND", "JMD": "JAMAICA DOLLAR", "JOD": "JORDAN DINAR", "JPY": "JAPAN YEN",
                  "KES": "KENYA SHILLING", "KGS": "KYRGYZSTAN SOM", "KHR": "CAMBODIA RIEL", "KMF": "COMOROS FRANC",
                  "KPW": "NORTH KOREA WON", "KRW": "SOUTH KOREA WON", "KWD": "KUWAIT DINAR",
                  "KYD": "CAYMAN ISLANDS DOLLAR", "KZT": "KAZAKHSTAN TENGE", "LAK": "LAOS KIP",
                  "LBP": "LEBANON POUND", "LKR": "SRI LANKA RUPEE", "LRD": "LIBERIA DOLLAR", "LSL": "LESOTHO LOTI",
                  "LYD": "LIBYA DINAR", "MAD": "MOROCCO DIRHAM", "MDL": "MOLDOVA LEU", "MGA": "MADAGASCAR ARIARY",
                  "MKD": "MACEDONIA DENAR", "MMK": "MYANMAR (BURMA) KYAT", "MNT": "MONGOLIA TUGHRIK",
                  "MOP": "MACAU PATACA", "MRO": "MAURITANIA OUGUIYA", "MUR": "MAURITIUS RUPEE",
                  "MVR": "MALDIVES (MALDIVE ISLANDS) RUFIYAA", "MWK": "MALAWI KWACHA", "MXN": "MEXICO PESO",
                  "MYR": "MALAYSIA RINGGIT", "MZN": "MOZAMBIQUE METICAL", "NAD": "NAMIBIA DOLLAR",
                  "NGN": "NIGERIA NAIRA", "NIO": "NICARAGUA CORDOBA", "NOK": "NORWAY KRONE", "NPR": "NEPAL RUPEE",
                  "NZD": "NEW ZEALAND DOLLAR", "OMR": "OMAN RIAL", "PAB": "PANAMA BALBOA", "PEN": "PERU SOL",
                  "PGK": "PAPUA NEW GUINEA KINA", "PHP": "PHILIPPINES PESO", "PKR": "PAKISTAN RUPEE",
                  "PLN": "POLAND ZLOTY", "PYG": "PARAGUAY GUARANI", "QAR": "QATAR RIYAL", "RON": "ROMANIA NEW LEU",
                  "RSD": "SERBIA DINAR", "RUB": "RUSSIA RUBLE", "RWF": "RWANDA FRANC", "SAR": "SAUDI ARABIA RIYAL",
                  "SBD": "SOLOMON ISLANDS DOLLAR", "SCR": "SEYCHELLES RUPEE", "SDG": "SUDAN POUND",
                  "SEK": "SWEDEN KRONA", "SGD": "SINGAPORE DOLLAR", "SHP": "SAINT HELENA POUND",
                  "SLL": "SIERRA LEONE LEONE", "SOS": "SOMALIA SHILLING", "SPL": "SEBORGA LUIGINO",
                  "SRD": "SURINAME DOLLAR", "STD": "SÃO TOMÉ AND PRÍNCIPE DOBRA", "SVC": "EL SALVADOR COLON",
                  "SYP": "SYRIA POUND", "SZL": "SWAZILAND LILANGENI", "THB": "THAILAND BAHT",
                  "TJS": "TAJIKISTAN SOMONI", "TMT": "TURKMENISTAN MANAT", "TND": "TUNISIA DINAR",
                  "TOP": "TONGA PA'ANGA", "TRY": "TURKEY LIRA", "TTD": "TRINIDAD AND TOBAGO DOLLAR",
                  "TVD": "TUVALU DOLLAR", "TWD": "TAIWAN NEW DOLLAR", "TZS": "TANZANIA SHILLING",
                  "UAH": "UKRAINE HRYVNIA", "UGX": "UGANDA SHILLING", "USD": "UNITED STATES DOLLAR",
                  "UYU": "URUGUAY PESO", "UZS": "UZBEKISTAN SOM", "VEF": "VENEZUELA BOLIVAR",
                  "VND": "VIET NAM DONG", "VUV": "VANUATU VATU", "WST": "SAMOA TALA",
                  "XAF": "COMMUNAUTÉ FINANCIÈRE AFRICAINE (BEAC) CFA FRANC BEAC", "XCD": "EAST CARIBBEAN DOLLAR",
                  "XDR": "INTERNATIONAL MONETARY FUND (IMF) SPECIAL DRAWING RIGHTS",
                  "XOF": "COMMUNAUTÉ FINANCIÈRE AFRICAINE (BCEAO) FRANC",
                  "XPF": "COMPTOIRS FRANÇAIS DU PACIFIQUE (CFP) FRANC", "YER": "YEMEN RIAL",
                  "ZAR": "SOUTH AFRICA RAND", "ZMW": "ZAMBIA KWACHA", "ZWD": "ZIMBABWE DOLLAR"}

'''
- country to currency-code mapping: courtesy www.iban.com
- key is Amazon.COUNTRY type
- for United States (U.S.A, USA, who sA, the US A, US A, USC) has multiple entries based on my testing of Alexa
'''

COUNTRY_CODES = {'FIJI': 'FJD', 'GHANA': 'GHS', 'IRAN': 'IRR', 'PAPUA NEW GUINEA': 'PGK', 'TURKEY': 'TRY',
                 'ECUADOR': 'USD', 'HAITI': 'HTG', 'ALGERIA': 'DZD', 'SERBIA': 'RSD', 'SAINT KITTS AND NEVIS': 'XCD',
                 'CHAD': 'XAF', 'SISTEMA UNITARIO DE COMPENSACION REGIONAL DE PAGOS "SUCRE"': 'XSU',
                 'NORFOLK ISLAND': 'AUD', 'YEMEN': 'YER', 'BOLIVARIAN REPUBLIC OF VENEZUELA': 'VEF', 'TUVALU': 'AUD',
                 'BONAIRE, SINT EUSTATIUS AND SABA': 'USD', 'INDONESIA': 'IDR', 'UNITED ARAB EMIRATES': 'AED',
                 'BOLIVIA': 'BOB', 'SAINT MARTIN': 'EUR', 'SLOVAKIA': 'EUR', 'VIRGIN ISLANDS': 'USD', 'ETHIOPIA': 'ETB',
                 'BRAZIL': 'BRL', 'TUNISIA': 'TND', 'UNITED KINGDOM': 'GBP', 'U.K': 'GBP', 'THE UNITED KINGDOM': 'GBP', 'AUSTRALIA': 'AUD',
                 'GUADELOUPE': 'EUR', 'THE UNITED ARAB EMIRATES': 'AED', 'NORWAY': 'NOK', 'LEBANON': 'LBP',
                 'MONACO': 'EUR', 'THE REPUBLIC OF KOREA': 'KRW', 'KUWAIT': 'KWD',
                 'LAO PEOPLE’S DEMOCRATIC REPUBLIC (THE)': 'LAK', 'BARBADOS': 'BBD', 'THE REPUBLIC OF MOLDOVA': 'MDL',
                 'PHILIPPINES': 'PHP', 'GUINEA': 'GNF', 'NEW CALEDONIA': 'XPF', 'FRENCH POLYNESIA': 'XPF',
                 'SAO TOME AND PRINCIPE': 'STD', 'ANGOLA': 'AOA', 'CHINA': 'CNY', 'MYANMAR': 'MMK', 'RÉUNION': 'EUR',
                 'NIUE': 'NZD', 'PUERTO RICO': 'USD', 'UK': 'GBP', 'BOTSWANA': 'BWP', 'LIBYA': 'LYD', 'AUSTRIA': 'EUR',
                 'ERITREA': 'ERN', 'COSTA RICA': 'CRC', 'SVALBARD AND JAN MAYEN': 'NOK', 'AZERBAIJAN': 'AZN',
                 'THE COMOROS': 'KMF', 'ANDORRA': 'EUR', 'THE COCOS (KEELING) ISLANDS': 'AUD', 'NEPAL': 'NPR',
                 'UNITED STATES MINOR OUTLYING ISLANDS': 'USD', 'THE SUDAN': 'SDG', 'GUATEMALA': 'GTQ',
                 'CHRISTMAS ISLAND': 'AUD', 'SUDAN': '', 'BURKINA FASO': 'XOF', 'U.S.A': 'USD', 'USA': 'USD', 'who sA': 'USD', 'the US A': 'USD', 'US A': 'USD', 'USC': 'USD', 'NICARAGUA': 'NIO',
                 'GREENLAND': 'DKK', 'THE DEMOCRATIC REPUBLIC OF THE CONGO': 'CDF', 'HUNGARY': 'HUF', 'CHILE': 'CLP',
                 'THE MARSHALL ISLANDS': 'USD', 'SAINT LUCIA': 'XCD', 'JERSEY': 'GBP', 'PALESTINE': 'USD',
                 'MACEDONIA': 'MKD', 'THE BAHAMAS': 'BSD', 'CENTRAL AFRICAN REPUBLIC': 'XAF', 'GUINEA-BISSAU': 'XOF',
                 'UZBEKISTAN': 'UZS', 'THE NETHERLANDS': 'EUR', 'TONGA': 'TOP', 'ZAMBIA': 'ZMW', 'BENIN': 'XOF',
                 'ARUBA': 'AWG', 'PALAU': 'USD', 'UNITED STATES OF AMERICA': 'USD', 'MONGOLIA': 'MNT',
                 'SOUTH KOREA': 'KRW', 'OMAN': 'OMR', 'TIMOR-LESTE': 'USD', 'SOLOMON ISLANDS': 'SBD',
                 'EL SALVADOR': 'USD', 'GUYANA': 'GYD', 'TRINIDAD AND TOBAGO': 'TTD', 'VANUATU': 'VUV',
                 'KYRGYZSTAN': 'KGS', 'PAKISTAN': 'PKR', 'THE PHILIPPINES': 'PHP', 'GRENADA': 'XCD',
                 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS': 'USD', 'ARGENTINA': 'ARS', 'NAMIBIA': 'NAD',
                 'CAMEROON': 'XAF', 'SAMOA': 'WST', 'THE NORTHERN MARIANA ISLANDS': 'USD', 'FRANCE': 'EUR',
                 'SAINT VINCENT AND THE GRENADINES': 'XCD', 'ISRAEL': 'ILS', 'BOSNIA AND HERZEGOVINA': 'BAM',
                 'COLOMBIA': 'COU', 'SWEDEN': 'SEK', 'MONTENEGRO': 'EUR', 'BELIZE': 'BZD', 'MAURITANIA': 'MRO',
                 'BRITISH INDIAN OCEAN TERRITORY': 'USD', 'THE NIGER': 'XOF', 'CURAÇAO': 'ANG',
                 'RUSSIAN FEDERATION': 'RUB', 'MALAYSIA': 'MYR', 'MOZAMBIQUE': 'MZN', 'EQUATORIAL GUINEA': 'XAF',
                 'IRELAND': 'EUR', 'WALLIS AND FUTUNA': 'XPF', 'ARMENIA': 'AMD', 'VIETNAM': 'VND', 'NAURU': 'AUD',
                 'MARTINIQUE': 'EUR', 'WESTERN SAHARA': 'MAD', 'TURKMENISTAN': 'TMT', 'FINLAND': 'EUR',
                 'ZIMBABWE': 'ZWL', 'THE FAROE ISLANDS': 'DKK', 'RWANDA': 'RWF', 'SAINT PIERRE AND MIQUELON': 'EUR',
                 'AFGHANISTAN': 'AFN', 'KENYA': 'KES', "CÔTE D'IVOIRE": 'XOF', 'BRUNEI DARUSSALAM': 'BND',
                 'BELGIUM': 'EUR', 'ÅLAND ISLANDS': 'EUR', 'SOUTH SUDAN': 'SSP', 'ALBANIA': 'ALL', 'DENMARK': 'DKK',
                 'LATVIA': 'EUR', 'VENEZUELA': 'VEF', 'GERMANY': 'EUR', 'GEORGIA': 'GEL', 'DOMINICA': 'XCD',
                 'TOGO': 'XOF', 'UKRAINE': 'UAH', 'ROMANIA': 'RON', 'MOROCCO': 'MAD', 'KIRIBATI': 'AUD',
                 'MADAGASCAR': 'MGA', 'PANAMA': 'USD', 'QATAR': 'QAR', 'THE FRENCH SOUTHERN TERRITORIES': 'EUR',
                 'SAN MARINO': 'EUR', 'THE GAMBIA': 'GMD', 'JAPAN': 'JPY', 'LIBERIA': 'LRD', 'SLOVENIA': 'EUR',
                 'BERMUDA': 'BMD', 'THE HOLY SEE': 'EUR', 'SIERRA LEONE': 'SLL', 'THE TURKS AND CAICOS ISLANDS': 'USD',
                 'CROATIA': 'HRK', 'GABON': 'XAF', 'SOMALIA': 'SOS', 'SRI LANKA': 'LKR', 'SINGAPORE': 'SGD',
                 'MAYOTTE': 'EUR', 'FRENCH GUIANA': 'EUR', 'NEW ZEALAND': 'NZD', 'SAINT BARTHÉLEMY': 'EUR',
                 'THE FALKLAND ISLANDS': 'FKP', 'BAHRAIN': 'BHD', 'CYPRUS': 'EUR', 'SAUDI ARABIA': 'SAR',
                 'POLAND': 'PLN', 'CAYMAN ISLANDS': 'KYD', 'EGYPT': 'EGP', 'SPAIN': 'EUR', 'MALDIVES': 'MVR',
                 'ICELAND': 'ISK', 'BHUTAN': 'INR', 'SURINAME': 'SRD', 'GIBRALTAR': 'GIP', 'BRITAIN': 'GBP',
                 'HONDURAS': 'HNL', 'CABO VERDE': 'CVE', 'PARAGUAY': 'PYG', 'SEYCHELLES': 'SCR', 'KOREA SOUTH': 'KRW',
                 'EUROPEAN UNION': 'EUR', 'JORDAN': 'JOD', 'ANTARCTICA': 'USD', 'THE COOK ISLANDS': 'NZD',
                 'ANTIGUA AND BARBUDA': 'XCD', 'ESTONIA': 'EUR', 'THE CZECH REPUBLIC': 'CZK', 'UGANDA': 'UGX',
                 'THE CENTRAL AFRICAN REPUBLIC': 'XAF', 'MALI': 'XOF', 'CONGO': 'XAF', 'BELARUS': 'BYR',
                 'MONTSERRAT': 'XCD', 'GREECE': 'EUR', 'THE RUSSIAN FEDERATION': 'RUB', 'LITHUANIA': 'EUR',
                 'PERU': 'PEN', 'SWAZILAND': 'SZL', 'MALAWI': 'MWK', 'BANGLADESH': 'BDT', 'ANGUILLA': 'XCD',
                 'THE DOMINICAN REPUBLIC': 'DOP', 'ITALY': 'EUR', 'IMF': 'XDR', 'SENEGAL': 'XOF', 'INDIA': 'INR',
                 'LESOTHO': 'ZAR', 'GUAM': 'USD', 'CUBA': 'CUP', 'NETHERLANDS': 'EUR', 'NIGERIA': 'NGN',
                 'JAMAICA': 'JMD', 'SWITZERLAND': 'CHW', 'MALTA': 'EUR', 'TAJIKISTAN': 'TJS', 'TANZANIA': 'TZS',
                 'CAMBODIA': 'KHR', 'PITCAIRN': 'NZD', 'HONG KONG': 'HKD', 'AMERICAN SAMOA': 'USD',
                 'GREAT BRITAIN': 'GBP', 'THAILAND': 'THB', 'SYRIAN ARAB REPUBLIC': 'SYP', 'US': 'USD',
                 'LUXEMBOURG': 'EUR', 'BULGARIA': 'BGN', 'URUGUAY': 'UYU', 'INTERNATIONAL MONETARY FUND': 'XDR',
                 'SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA': 'SHP', 'BURUNDI': 'BIF',
                 'THE DEMOCRATIC PEOPLE’S REPUBLIC OF KOREA': 'KPW', 'CANADA': 'CAD', 'UAE': 'AED',
                 'BOUVET ISLAND': 'NOK', 'SOUTH AFRICA': 'ZAR', 'KAZAKHSTAN': 'KZT', 'MEXICO': 'MXN',
                 'ISLAMIC REPUBLIC OF IRAN': 'IRR', 'IRAQ': 'IQD', 'ISLE OF MAN': 'GBP', 'TOKELAU': 'NZD',
                 'GUERNSEY': 'GBP', 'HEARD ISLAND AND McDONALD ISLANDS': 'AUD', 'MACAO': 'MOP', 'PORTUGAL': 'EUR',
                 'RUSSIA': 'RUB', 'FEDERATED STATES OF MICRONESIA': 'USD', 'UNITED REPUBLIC OF TANZANIA': 'TZS',
                 'TAIWAN': 'TWD', 'LIECHTENSTEIN': 'CHF', 'THE CAYMAN ISLANDS': 'KYD', 'DJIBOUTI': 'DJF',
                 'MAURITIUS': 'MUR'}


def lambda_handler(event, context):
    if (event["session"]["application"]["applicationId"] !=
            "amzn1.ask.skill.e5bff515-cd3a-4bb2-99b4-ceca575b81bf"):
        raise ValueError("Invalid Application!")

    if event["session"]["new"]:
        on_session_started({"requestId": event["request"]["requestId"]}, event["session"])

    if event["request"]["type"] == "LaunchRequest":
        return on_launch(event["request"], event["session"])
    elif event["request"]["type"] == "IntentRequest":
        return on_intent(event["request"], event["session"])
    elif event["request"]["type"] == "SessionEndedRequest":
        return on_session_ended(event["request"], event["session"])


def on_session_started(session_started_request, session):
    print
    "Starting new session."


def on_launch(launch_request, session):
    session_attributes = {}
    card_title = "Currency Conversion Welcome"
    speech_output = "Hello, ask me currency conversion from Country A to Country B. "\
                        "For example: China to INDIA."
    reprompt_text = "Please ask me currency conversion from Country A to Country B. " \
                        "For example, try asking France to India."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def on_intent(intent_request, session):
    intent = intent_request["intent"]
    intent_name = intent_request["intent"]["name"]
    print(intent_request)
    if intent_name == "convertCurrency":
        return get_currency_conversion(intent)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    print
    "Ending session."


def get_currency_conversion(intent):
    session_attributes = {}
    card_title = "Currency Conversion"
    speech_output = "Sorry! I am not sure of the currency conversion Country you requested for. " \
                    "Try asking Country A to Country B."
    reprompt_text = "Sorry! I am not sure of the currency conversion Country you requested for. " \
                    "For example, try asking USA to india"
    should_end_session = True

    if "CurrencyFrom" in intent["slots"] and "CurrencyTo" in intent["slots"] and "value" in intent["slots"]["CurrencyFrom"] and "value" in intent["slots"]["CurrencyTo"]:
        from_country = intent["slots"]["CurrencyFrom"]["value"]
        to_country = intent["slots"]["CurrencyTo"]["value"]

        print("currency country from {} to {}".format(from_country, to_country))

        if from_country is not None and to_country is not None:
            from_country = from_country.upper()
            to_country = to_country.upper()
            from_currency_code = get_currency_code(from_country)
            to_currency_code = get_currency_code(to_country)

            print("currency code from {} to {}".format(from_currency_code, to_currency_code))

            if from_currency_code is not None and to_currency_code is not None:
                from_currency_name = get_currency(from_currency_code)
                to_currency_name = get_currency(to_currency_code)

                print("currency names from {} to {}".format(from_currency_name, to_currency_name))

                try:
                    # sample: '{"base":"USD","date":"2017-04-28","rates":{"INR":64.284}}'
                    resp = requests.get(
                        "http://api.fixer.io/latest",
                        params={"base": from_currency_code, "symbols": to_currency_code})
                    jsn_resp = json.loads(resp.content.decode('utf-8'))

                    if len(jsn_resp['rates']) == 0:
                        raise Exception

                    speech_output = "As of {}, one {} fetches {} {}".format(jsn_resp['date'], from_currency_name,
                                                                                jsn_resp['rates'][to_currency_code],
                                                                             to_currency_name)
                except Exception:
                    print("fixer.io exception - trying with currencyconverterapi.com")
                    # sample: {"USD_INR":{"val":64.269}}
                    resp = requests.get(
                        "http://free.currencyconverterapi.com/api/v3/convert?compact=y",
                        params={"q": from_currency_code + "_" + to_currency_code})
                    jsn_resp = json.loads(resp.content.decode('utf-8'))

                    speech_output = "One {} fetches {} {}".format(from_currency_name,
                                                                  jsn_resp[from_currency_code + "_" + to_currency_code][
                                                                      'val'],
                                                                  to_currency_name)

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_currency_code(country):
    try:
        return COUNTRY_CODES[country]
    except KeyError:
        print("invlaid country {}".format(country))
        return None


def get_currency(currency_code):
    try:
        return CODES_CURRENCY[currency_code]
    except KeyError:  # return None if key is not found
        print("invlaid curreny code {}".format(currency_code))
        return None


def handle_session_end_request():
    speech_output = "Thank you for using currency conversion skill!!"
    card_title = "Currency Conversion End"
    should_end_session = True

    return build_response({}, build_speechlet_response(card_title, speech_output, None, should_end_session))


def build_speechlet_response(card_title, output, reprompt_text, should_end_session):
    return {
        "outputSpeech": {
            "type": "PlainText",
            "text": output
        },
        "card": {
            "type": "Simple",
            "title": card_title,
            "content": output
        },
        "reprompt": {
            "outputSpeech": {
                "type": "PlainText",
                "text": reprompt_text
            }
        },
        "shouldEndSession": should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        "version": "1.0",
        "sessionAttributes": session_attributes,
        "response": speechlet_response
    }


def get_welcome_response():
    speech_output = "Currency conversion skill lets you know the converted currency value from Country A to Country B. For example, you can ask currency conversion from usa to india. Now, what can I help you with?"
    card_title = "Currency Conversion Help"
    should_end_session = False
    return build_response({}, build_speechlet_response(card_title, speech_output, None, should_end_session))
