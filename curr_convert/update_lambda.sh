#!/bin/bash

if [ $# -eq 2 ]
then
    echo "*** deleting previous $2.zip file"
    rm $2.zip

    echo "*** packaging new $2.zip file"
    zip -r $2.zip .

    echo "*** updating lambda function $1"
    aws lambda update-function-code --function-name $1 --zip-file fileb://$2.zip

    echo "*** END ***"
else
    echo ""
    echo "**************************************************************************************************"
    echo "update_lambda.sh needs two parameters. Example: ./update_lambda.sh lambda-function-name zip-file-name"
    echo "**************************************************************************************************"
    echo ""
fi

