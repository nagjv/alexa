

def lambda_handler(event, context):
    if (event["session"]["application"]["applicationId"] !=
            "amzn1.ask.skill.40206adb-c2eb-xxxx-xxxx-xxxxxxxxxxxx"):
        raise ValueError("Invalid Application!")

    if event["session"]["new"]:
        on_session_started({"requestId": event["request"]["requestId"]}, event["session"])

    if event["request"]["type"] == "LaunchRequest":
        return on_launch(event["request"], event["session"])
    elif event["request"]["type"] == "IntentRequest":
        return on_intent(event["request"], event["session"])
    elif event["request"]["type"] == "SessionEndedRequest":
        return on_session_ended(event["request"], event["session"])


def on_session_started(session_started_request, session):
    print
    "Starting new session."


def on_launch(launch_request, session):
    session_attributes = {}
    card_title = "Multy Table Welcome"
    speech_output = "Hello, ask me multiplication table for a number between 1 and 20."
    reprompt_text = "Please ask me multiplication table for a number between 1 and 10." \
                    "For example, try asking multiplication table for 3."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def on_intent(intent_request, session):
    intent = intent_request["intent"]
    intent_name = intent_request["intent"]["name"]

    if intent_name == "GetMultiplicationTableByNumber":
        return get_multiplication_table_by_number(intent)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    print
    "Ending session."


def get_multiplication_table_by_number(intent):
    session_attributes = {}
    card_title = "Multiplication table"
    speech_output = "Sorry! I am not sure which number you wanted multiplication table for." \
                    "Please try again."
    reprompt_text = "Sorry! I am not sure which number you wanted multiplication table for." \
                    "For example, try asking multiplication table for 5."
    should_end_session = False

    if "TableFor" in intent["slots"]:
        multy_table_for = intent["slots"]["TableFor"]["value"]
        speech_output = "Here you go, multiplication table for "
        if(multy_table_for.isdigit()):
            speech_output += multy_table_for + ":"
            multy_table_for = int(multy_table_for)
            for i in range(1, 11):
                speech_output += " {} times {} is {},".format(multy_table_for, i, (multy_table_for * i))
            speech_output = speech_output.rstrip(",")
            speech_output += ". If you wish to continue, say a number between 1 and 20."
        else:
            speech_output = "Sorry! I didn't get the number. Please try again." \
                    "For example, try asking multiplication table for 5."

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    speech_output = "Keep practicing!!! Math is fun and math makes you smarter."
    card_title = "Multy Table End"
    should_end_session = True

    return build_response({}, build_speechlet_response(card_title, speech_output, None, should_end_session))


def build_speechlet_response(card_title, output, reprompt_text, should_end_session):
    return {
        "outputSpeech": {
            "type": "PlainText",
            "text": output
        },
        "card": {
            "type": "Simple",
            "title": card_title,
            "content": output
        },
        "reprompt": {
            "outputSpeech": {
                "type": "PlainText",
                "text": reprompt_text
            }
        },
        "shouldEndSession": should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        "version": "1.0",
        "sessionAttributes": session_attributes,
        "response": speechlet_response
    }